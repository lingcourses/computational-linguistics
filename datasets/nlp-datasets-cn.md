## **数据集**

[Apache Software Foundation公共邮件存档](https://link.zhihu.com/?target=https%3A//aws.amazon.com/de/datasets/apache-software-foundation-public-mail-archives/)：截至2011年7月11日所有公开可用的Apache Software Foundation邮件存档（200 GB）

[博客作者身份语料库](https://link.zhihu.com/?target=http%3A//u.cs.biu.ac.il/~koppel/BlogCorpus.htm)：由2004年8月从blogger.com收集的19,320位博主的文章组成，共计681,288篇，字数超过1.4亿——平均每人35篇、7250字（298 MB）

[亚马逊食品评论[Kaggle]](https://link.zhihu.com/?target=https%3A//www.kaggle.com/snap/amazon-fine-food-reviews) ：数据跨越10年以上，包括截至2012年10月的568,454条评论。内容包括产品、用户信息、评分以及纯文本评论（240 MB）。附：[斯坦福大学论文](https://link.zhihu.com/?target=http%3A//i.stanford.edu/~julian/pdfs/www13.pdf)

[亚马逊评论](https://link.zhihu.com/?target=https%3A//snap.stanford.edu/data/web-Amazon.html)：斯坦福收集了3500万条亚马逊评论，跨度18年（11 GB）

[arXiv](https://link.zhihu.com/?target=https%3A//arxiv.org/help/bulk_data_s3)：所有归档的论文全文（270 GB）+源文件（190 GB）

[ASAP自动短文评分[Kaggle]](https://link.zhihu.com/?target=https%3A//www.kaggle.com/c/asap-aes/data)：共八个作文集，每一集作文都围绕一个主题展开。短文的平均长度为150到550个字。一些文章依赖于主题信息，另一些则是自由发挥。所有文章都是由7年级到10年级的学生撰写的，并经相关人员手工评分，有些还进行了双重评分（100 MB）

[ASAP自动简答题评分](https://link.zhihu.com/?target=https%3A//www.kaggle.com/c/asap-sas/data)：共十个数据集，每个数据集都是由单个提示生成的。平均长度为50个字。一些回答依赖于问题信息，另一些则是自由发挥。所有答案都是由10年级的学生撰写的，并经相关人员手动分级并进行双重评分（35 MB）

[美国政客的社交媒体消息分类](https://link.zhihu.com/?target=https%3A//www.figure-eight.com/data-for-everyone/)：收集了来自美国参议员和其他美国政客的数千条社交媒体消息，可按内容分类为目标群众（国家或选民）、政治主张（中立/两党或偏见/党派）和实际内容（如攻击政敌等）（4 MB）_注：该网站还有其他大量CV、NLP和语音方面的小型数据集_

[CSI语料库](https://link.zhihu.com/?target=https%3A//www.clips.uantwerpen.be/datasets/csi-corpus)：荷兰语，该语料库包含两种类型的学生文本：作文和评论。涉及作者（性别、年龄、性取向、来源地区、性格概况）和文档（时间、流派、真实性、情绪、等级）等大量元数据。由安特卫普大学CLiPS研究中心提供，主要用于计量文体学分析。

[ClueWeb09 FACC](https://link.zhihu.com/?target=http%3A//lemurproject.org/clueweb09/FACC1/)：带有Freebase注释的[ClueWeb09](https://link.zhihu.com/?target=http%3A//lemurproject.org/clueweb09/)和[ClueWeb12](https://link.zhihu.com/?target=http%3A//lemurproject.org/clueweb12/)语料库（72 GB）

[ClueWeb11 FACC](https://link.zhihu.com/?target=http%3A//lemurproject.org/clueweb12/FACC1/)：带有Freebase标识符注释的[ClueWeb11](https://link.zhihu.com/?target=http%3A//lemurproject.org/clueweb12/)（92 GB）

[AWS爬虫数据](https://link.zhihu.com/?target=https%3A//aws.amazon.com/cn/public-datasets/common-crawl/)：收集了从2008以来抓取的50亿个网页的数据。其中自2013年开始，所有爬虫只持续一个月，数据以WARC文件格式存储。从2012年开始，抓取的数据还包含元数据（WAT）和文本数据（WET）提取，大大简化了数据处理（541 TB）

[康奈尔电影对话语料库（Cornell Movie Dialog Corpus）](https://link.zhihu.com/?target=http%3A//www.cs.cornell.edu/~cristian/Cornell_Movie-Dialogs_Corpus.html)：包含从原始电影脚本中提取的虚构对话集：10,292对电影角色之间的220,579次会话交流、涉及617部电影中的9,035个字符，共304,713个句子。元数据极其丰富，包含流派、发布年份、IMDB评级、IMDB票数、性别、在电影积分榜上的位置。

[crosswikis](https://link.zhihu.com/?target=https%3A//nlp.stanford.edu/data/crosswikis-data.tar.bz2/)：英语短语相关的维基百科文章数据库、论文（11 GB）

[DBpedia](https://link.zhihu.com/?target=https%3A//aws.amazon.com/de/datasets/dbpedia-3-5-1/%3Ftag%3Ddatasets%2523keywords%2523encyclopedic)：包含从维基百科中提取出的结构化信息，包括312,000个人、413,000个地点、94,000张音乐专辑、49,000部电影、15,000种电子游戏、140,000个组织、146,000个物种和4600种疾病。共计10亿多条信息，其中2.57亿条来自维基百科英文版，7.66亿条来自其他语言版本（17 GB）

[Death Row](https://link.zhihu.com/?target=http%3A//www.tdcj.state.tx.us/death_row/dr_executed_offenders.html)：包含美国德州自1984年以来每个执行死刑罪犯的遗言（HTML表格）

[Del.icio.us](https://link.zhihu.com/?target=https%3A//arvindn.livejournal.com/116137.html)：包含delicious.com上的125万个书签（170 MB）

[社交媒体上有关灾难的消息](https://link.zhihu.com/?target=https%3A//www.figure-eight.com/data-for-everyone/)：包含10,000条和灾难事故相关的带注释推特（2 MB）

[经济新闻报道的基调和相关性](https://link.zhihu.com/?target=https%3A//www.figure-eight.com/data-for-everyone)：包含从1951年到2014年的经济新闻，可根据新闻报道判断该文章是否与美国经济情况相关，如果是，报道的基调是什么（12 MB）

[Enron电子邮件集](https://link.zhihu.com/?target=https%3A//aws.amazon.com/de/datasets/enron-email-data/)：包含1,227,255封电子邮件，其中493,384份附件覆盖了151名托管人。该电子邮件的格式为Microsoft PST、IETF MIME和EDRM XML（210 GB）

[Event Registry](https://link.zhihu.com/?target=http%3A//eventregistry.org/)：可以实时访问全球100,000个新闻源的新闻文章，有API（免费查询工具）

[垃圾邮件/标题党新闻数据集[Kaggle]](https://link.zhihu.com/?target=https%3A//www.kaggle.com/therohk/examine-the-examiner)：新闻网站The Examiner上的新闻汇编，包含超过6年的21000多位作者撰写的300万篇文章的标题（200 MB）

[联邦采购数据中心的联邦合同](https://link.zhihu.com/?target=https%3A//aws.amazon.com/de/datasets/federal-contracts-from-the-federal-procurement-data-center-usaspending-gov/)：来自管理联邦采购数据系统（FPDS-NG）的联邦采购数据中心（FPDC）的转储，真实性和准确性已受承诺（180 GB）

[Flickr Personal Taxonomies](https://link.zhihu.com/?target=https%3A//www.isi.edu/~lerman/downloads/flickr/flickr_taxonomies.html)：社交媒体上用户按个人喜好分类内容的树形数据集，包含7,121位Flickr的树（40 MB）

[Freebase数据转储](https://link.zhihu.com/?target=https%3A//aws.amazon.com/de/datasets/freebase-data-dump/)：是一个开放的世界信息数据库，包含电影、音乐、人物、地域在内的数百个类别的数百万个主题（26GB）

[Freebase简单主题转储](https://link.zhihu.com/?target=https%3A//aws.amazon.com/de/datasets/freebase-simple-topic-dump/)：关于Freebase中每个主题的基本识别事实的数据转储（5 GB）

[Freebase Quad Dump](https://link.zhihu.com/?target=https%3A//aws.amazon.com/de/datasets/freebase-quad-dump/)：Freebase中所有当前事实和主张的数据转储（35 GB）

[GigaOM Wordpress Challenge [Kaggle]](https://link.zhihu.com/?target=https%3A//www.kaggle.com/c/predict-wordpress-likes/data)：预测有人会喜欢哪些博客文章。包含博客文章、元数据、用户喜欢情况等信息（1.5 GB）

[Google Books Ngrams](https://link.zhihu.com/?target=http%3A//storage.googleapis.com/books/ngrams/books/datasetsv2.html)：包含在整个语料库中出现超过40次的n-gram，优化了快速查询小组短语的用法（2.2 TB）

[Google Web 5gram](https://link.zhihu.com/?target=https%3A//catalog.ldc.upenn.edu/LDC2006T13)：包含n-gram及其观察到的频率计数。n-gram的长度从unigrams（单个单词）到5-gram，主要用于统计语言建模（24 GB）

[Gutenberg EBooks](https://link.zhihu.com/?target=http%3A//www.gutenberg.org/wiki/Gutenberg%3AOffline_Catalogs)：电子书基本信息的注释列表（2 MB）

[哈佛图书馆](https://link.zhihu.com/?target=http%3A//library.harvard.edu/open-metadata%23Harvard-Library-Bibliographic-Dataset)：哈佛图书馆藏书记录已超过1,200万册，包括书籍、期刊、电子资料、手稿、档案资料、乐谱、音频、视频和其他资料（4GB）

[仇恨言语识别](https://link.zhihu.com/?target=https%3A//github.com/t-davidson/hate-speech-and-offensive-language)：ICWSM 2017论文“自动仇恨语音检测和无礼语言问题”的作者贡献。包含3类短文本：a）包含仇恨言论；b）是冒犯性的，但没有仇恨言论；c）根本没有冒犯性。由15,000行文本构成，每个字符串都经过3人判断（3 MB）

[希拉里克林顿的电子邮件[Kaggle]](https://link.zhihu.com/?target=https%3A//www.kaggle.com/kaggle/hillary-clinton-emails)：美国国务院发布的近7,000页的希拉里·克林顿精心编辑的电子邮件（12 MB）

[Home Depot产品搜索相关性](https://link.zhihu.com/?target=https%3A//www.kaggle.com/c/home-depot-product-search-relevance/data)[Kaggle]：包含Home Depot网站上的许多产品和真实客户的搜索关键词。每对词都经3名评估人员评估，并给出1—3的相关性评分，可用来预测相关性（65 MB）

[识别文本中的关键短语](https://link.zhihu.com/?target=https%3A//www.figure-eight.com/data-for-everyone/)：Question / Answer pairs + context；如果与问题/答案有关，则判断上下文关系（8 MB）

[Jeopardy](https://link.zhihu.com/?target=https%3A//www.reddit.com/r/datasets/comments/1uyd0t/200000_jeopardy_questions_in_a_json_file/)：包含216,930个危险问题（53 MB）

[20万英语笑话文本](https://link.zhihu.com/?target=https%3A//github.com/taivop/joke-dataset)：来源于各个地方的208,000个纯文本笑话

[欧洲语言机器翻译](https://link.zhihu.com/?target=http%3A//statmt.org/wmt11/translation-task.html%23download)：（612 MB）

[材料安全数据表](https://link.zhihu.com/?target=https%3A//aws.amazon.com/de/datasets/material-safety-data-sheets/)：230,000份材料安全数据表，包含化学成分、急救措施、储存和处理等信息（3 GB）

[澳大利亚新闻标题[Kaggle]](https://link.zhihu.com/?target=https%3A//www.kaggle.com/therohk/million-headlines)：包含15年内（2003年初至2017年）澳大利亚广播公司发布的130万条新闻的标题，深入研究关键词，可以看到所有塑造了过去十年的重要事件，以及它们随着时间的演变历程（56 MB）

[MCTest](https://link.zhihu.com/?target=https%3A//www.microsoft.com/en-us/research/lab/microsoft-research-redmond/%3Ffrom%3Dhttp%253A%252F%252Fresearch.microsoft.com%252Fen-us%252Fum%252Fredmond%252Fprojects%252Fmctest%252Findex.html)：免费提供一组660个故事和相关问题，用于研究机器对文本的理解、回答问题（1 MB）

[NEGRA](https://link.zhihu.com/?target=http%3A//www.coli.uni-saarland.de/projects/sfb378/negra-corpus/negra-corpus.html)：德语报刊文本的句法注释语料库，适用于所有大学和非营利组织，需要签署并发送表格才能获得

[印度新闻标题[Kaggle]](https://link.zhihu.com/?target=https%3A//www.kaggle.com/therohk/india-headlines-news-dataset)：汇编了2001年至2017年印度“泰晤士报”发表的270万条新闻的标题（185 MB）

[新闻文章/维基百科页面配对](https://link.zhihu.com/?target=https%3A//www.figure-eight.com/data-for-everyone/)：阅读一篇简短文章，并选出它和两篇维基百科文章中的哪一篇最接近（6 MB）

[NIPS2015论文（第2版）[Kaggle]](https://link.zhihu.com/?target=https%3A//www.kaggle.com/benhamner/nips-2015-papers/version/2)：所有NIPS2015论文全文（335 MB）

[NYTimes Facebook数据](https://link.zhihu.com/?target=http%3A//minimaxir.com/2015/07/facebook-scraper/)：所有《纽约时报》的Facebook帖子（5 MB）

[一周全球新闻馈送[Kaggle]](https://link.zhihu.com/?target=https%3A//www.kaggle.com/therohk/global-news-week)：一周内（2017年8月24日至2017年8月30日）全球在线发布的大多新闻内容的快照，包括大约140万篇文章、20,000个新闻来源和20多种语言（115 MB）

[句子/概念对的真实含义](https://link.zhihu.com/?target=https%3A//www.figure-eight.com/data-for-everyone/)：用两个概念来读一个句子，例如“一只狗是一种动物”或“船长可以与主人具有相同的含义”，判断这句话是否属实，然后将结果排列为1-5五个等级（700 KB）

[Open Library数据转储](https://link.zhihu.com/?target=https%3A//openlibrary.org/developers/dumps)：包含Open Library中所有记录的最新版本的转储（16 GB）

[Personae语料库](https://link.zhihu.com/?target=https%3A//www.clips.uantwerpen.be/datasets/personae-corpus)：收集用于作者信息和个性预测的实验，由145名不同学生编写的145篇荷兰语文章组成，每个学生还参加了在线MBTI性格测试

[Reddit评论](https://link.zhihu.com/?target=https%3A//www.reddit.com/r/datasets/comments/3bxlg7/i_have_every_publicly_available_reddit_comment/)：截至2015年7月的每个公开可用的书签评论，共计17亿条评论（250 GB）

[Reddit评论（15年5月）[Kaggle]](https://link.zhihu.com/?target=https%3A//www.kaggle.com/reddit/reddit-comments-may-2015)：上个数据集的子集（8 GB）

[Reddit推荐帖语料库](https://link.zhihu.com/?target=https%3A//www.reddit.com/r/datasets/comments/3mg812/full_reddit_submission_corpus_now_available_2006/)：从2006年1月至2015年8月31日所有公开可用的Reddit推荐帖（42 GB）

[路透社语料库](https://link.zhihu.com/?target=https%3A//trec.nist.gov/data/reuters/reuters.html)：包含大量路透社新闻报道，主要用于研究和开发自然语言处理、信息检索和机器学习系统。在2004年秋季，NIST接管了RCV1，所以现在需要向NIST发送请求并签署协议来获取这些数据集（2.5 GB）

[SaudiNewsNet](https://link.zhihu.com/?target=https%3A//github.com/ParallelMazen/SaudiNewsNet)：包含从各种在线沙特报纸中摘录的31,030份阿拉伯文报纸文章及其元数据（2 MB）

[短信垃圾邮件收集](https://link.zhihu.com/?target=http%3A//www.dt.fee.unicamp.br/~tiago/smsspamcollection/)：是一个包含5,574英文单词，真实的、未附带附件的短信内容集，已合法进行标记（200 KB）

[SouthparkData](https://link.zhihu.com/?target=https%3A//github.com/BobAdamsEE/SouthParkData)：带有脚本信息的.csv文件，包含《南方公园》季数、剧集、角色等信息（3.6 MB）

[Stackoverflow](https://link.zhihu.com/?target=http%3A//data.stackexchange.com/)：730万个stackoverflow问题+其他stackexchanges（查询工具）

[Twitter Cheng-Caverlee-Lee Scrape](https://link.zhihu.com/?target=https%3A//archive.org/details/twitter_cikm_2010)：包含2009年9月至2010年1月twitter收集的基于内容的用户地理定位信息，包含115,886位Twitter用户和3,844,612个位置更新：经度、纬度（400 MB）

[Twitter上关于新英格兰爱国者队泄气门（Deflategate）事件的情绪](https://link.zhihu.com/?target=https%3A//www.figure-eight.com/data-for-everyone/)：新英格兰爱国者队在美国橄榄球联合会（AFC）决赛中击败印第安纳波利斯小马队，将在2月1号的超级碗大赛中迎战西雅图海鹰队。但是爱国者队被发现在这次比赛中使用的12个橄榄球有11个充气不足。这个数据集可用于观察丑闻爆发Twitter用户的情绪，以衡量公众对整个事件的看法（2 MB）

[Twitter上激进分子情绪分析](https://link.zhihu.com/?target=https%3A//www.figure-eight.com/data-for-everyone/)：关于诸如堕胎合法化、女权主义、希拉里·克林顿等各种左倾问题的推文，如果所述推文对该问题赞成、反对或保持中立，则将其分类（600 KB）

[Twitter Sentiment140](https://link.zhihu.com/?target=http%3A//help.sentiment140.com/for-students/)：与品牌/关键字相关的推文，网站上包括论文和研究想思路（77 MB）

[Twitter的情绪分析](https://link.zhihu.com/?target=https%3A//www.figure-eight.com/data-for-everyone/)：自驾车：阅读推文，将其分为非常积极的、轻微积极的、中性的、轻微消极的或非常消极的，并标记其是否与自驾车相关（1 MB）

[Twitter东京地理定位推文](https://link.zhihu.com/?target=http%3A//followthehashtag.com/datasets/200000-tokyo-geolocated-tweets-free-twitter-dataset/)：来自东京的20万条推文（47 MB）

[Twitter UK Geolocated Tweets](https://link.zhihu.com/?target=http%3A//followthehashtag.com/datasets/170000-uk-geolocated-tweets-free-twitter-dataset/)：来自英国的17万条推文。（47 MB）

[Twitter美国地理定位推文](https://link.zhihu.com/?target=http%3A//followthehashtag.com/datasets/free-twitter-dataset-usa-200000-free-usa-tweets/)：来自美国的20万条推文（45 MB）

[Twitter美国航空公司情绪[Kaggle]](https://link.zhihu.com/?target=https%3A//www.kaggle.com/crowdflower/twitter-airline-sentiment)：收集了twitter用户对于一些美国主要航空公司的评价情况。数据始于从2015年2月，评论者需选择正面、负面和中性中的一类，如有负面评价，再进行原因分类（如“晚班”或“粗鲁服务”）（2.5 MB）

[基于新闻文章判断美国经济表现](https://link.zhihu.com/?target=https%3A//www.figure-eight.com/data-for-everyone/)：新闻文章的标题和摘要与美国经济相关程度的排名（5 MB）

[Urban Dictionary词汇和定义[Kaggle]](https://link.zhihu.com/?target=https%3A//www.kaggle.com/therohk/urban-dictionary-words-dataset)：截至2016年5月，包含全部260万个Urban Dictionary的词汇定义、提交者和点赞数量的CSV语料库（238 MB）

[WestburyLab USENET语料库](https://link.zhihu.com/?target=https%3A//aws.amazon.com/de/datasets/the-westburylab-usenet-corpus/)：2005—2010年47,860篇英语新闻的匿名汇总，文章长度在500字和500,000字之间，英文单词占比高达90%（40 GB）

[WestburyLab维基百科语料库（2010）](https://link.zhihu.com/?target=http%3A//www.psych.ualberta.ca/~westburylab/downloads/westburylab.wikicorp.download.html)：包含2010年4月以前维基百科英文部分中的所有文章的快照，已经去除了所有链接和不相关的材料（导航文本等），但未经标记，是原始文本（1.8 GB）

[WEX](https://link.zhihu.com/?target=https%3A//aws.amazon.com/de/datasets/wikipedia-extraction-wex/)：英文维基百科的处理转储（66 GB）

[维基百科XML数据](https://link.zhihu.com/?target=https%3A//aws.amazon.com/de/datasets/wikipedia-xml-data/)：维基媒体基金会提供的完整副本，以wikitext源代码和嵌入XML的元数据形式提供（500 GB）

[Yahoo! Answers Comprehensive Questions and Answers](https://link.zhihu.com/?target=https%3A//webscope.sandbox.yahoo.com/catalog.php%3Fdatatype%3Dl)：2017年10月25日创建，包含4,483,032个问题及其答案（3.6 GB）

[Yahoo! Answers consisting of questions asked in French](https://link.zhihu.com/?target=https%3A//webscope.sandbox.yahoo.com/catalog.php%3Fdatatype%3Dl)：Yahoo! Answers corpus from 2006 to 2015的子集，包含170万个法语问题以及相应的答案（3.8 GB）

[Yahoo! Answers Manner Questions](https://link.zhihu.com/?target=https%3A//webscope.sandbox.yahoo.com/catalog.php%3Fdatatype%3Dl)：Yahoo! Answers corpus的子集，并根据语言属性进行选择，包含142,627个问题及其答案。（104 MB）

[Yahoo!从公开可用网页中提取的HTML表单](https://link.zhihu.com/?target=https%3A//webscope.sandbox.yahoo.com/catalog.php%3Fdatatype%3Dl)：包含一小部分含有复杂HTML表单的页面，共计267万个复杂表单（50+ GB）

[Yahoo!从公开可用网页中提取元数据](https://link.zhihu.com/?target=https%3A//webscope.sandbox.yahoo.com/catalog.php%3Fdatatype%3Dl)：RDF数据（2 GB）

[Yahoo! N-Gram Representations](https://link.zhihu.com/?target=https%3A//webscope.sandbox.yahoo.com/catalog.php%3Fdatatype%3Dl)：包含n-gram表示形式，这些数据可以作为查询重写任务的测试平台，这是IR研究中的一个常见问题，也是NLP研究中常见的单词和句子相似性任务（2.6 GB）

[Yahoo! N-Grams 2.0](https://link.zhihu.com/?target=https%3A//webscope.sandbox.yahoo.com/catalog.php%3Fdatatype%3Dl)：n-gram（n = 1至5），从1260多个面向新闻的站点中检索到的1460万个文档（1.26亿条独特语句，34亿个运行词）（12 GB）

[Yahoo!搜索日志与相关性判断](https://link.zhihu.com/?target=https%3A//webscope.sandbox.yahoo.com/catalog.php%3Fdatatype%3Dl)：匿名化的Yahoo!搜索日志与相关性判断（1.3 GB）

[Yahoo!英语维基百科的语义注释快照](https://link.zhihu.com/?target=https%3A//webscope.sandbox.yahoo.com/catalog.php%3Fdatatype%3Dl)：英文维基百科从2006年11月4日开始使用一些公开的NLP工具进行处理，共1,490,688个条目（6 GB）

[Yelp](https://link.zhihu.com/?target=https%3A//www.yelp.com/dataset)：包括餐厅排名和220万条评论

[YouTube](https://link.zhihu.com/?target=https%3A//www.reddit.com/r/datasets/comments/3gegdz/17_millions_youtube_videos_description/)：170万个YouTube视频的简介

## **主要来源**

*   [awesome-public-datasets/ NLP](https://link.zhihu.com/?target=https%3A//github.com/awesomedata/awesome-public-datasets%23natural-language)（包含更多列表）
*   [AWS公开数据集](https://link.zhihu.com/?target=https%3A//aws.amazon.com/de/datasets/)
*   [CrowdFlower: Data for Everyone](https://link.zhihu.com/?target=https%3A//www.figure-eight.com/data-for-everyone/)（包含大量小型调查以及为特定任务众包获得的数据）
*   [Kaggle 1](https://link.zhihu.com/?target=https%3A//www.figure-eight.com/data-for-everyone/), [2](https://link.zhihu.com/?target=https%3A//www.kaggle.com/competitions)（需要确保该比赛数据可用于其他非竞赛场合）
*   [Open Library](https://link.zhihu.com/?target=https%3A//openlibrary.org/developers/dumps)
*   [Quora](https://link.zhihu.com/?target=https%3A//www.quora.com/Datasets-What-are-the-major-text-corpora-used-by-computational-linguists-and-natural-language-processing-researchers-and-what-are-the-characteristics-biases-of-each-corpus)（主要注释的语料库）
*   [/r/datasets](https://link.zhihu.com/?target=https%3A//www.reddit.com/r/datasets/)（无尽的数据集列表，大部分是由业余爱好者提供的，但没有正确记录或许可）
*   [rs.io](https://link.zhihu.com/?target=http%3A//rs.io/100-interesting-data-sets-for-statistics/)（另一张大列表）
*   [Stackexchange：Opendata](https://link.zhihu.com/?target=https%3A//opendata.stackexchange.com/)
*   [斯坦福大学NLP小组](https://link.zhihu.com/?target=https%3A//nlp.stanford.edu/links/statnlp.html)（主要注释语料库和TreeBanks，或提供实际的NLP工具）
*   [Yahoo! Webscope](https://link.zhihu.com/?target=https%3A//webscope.sandbox.yahoo.com/)（包含使用他们数据集的论文）